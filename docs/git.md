# Git Commands
[Git](https://en.wikipedia.org/wiki/Git) is a version control system (VCS) for tracking changes in computer files and coordinating 
work on those files among multiple people.

We recommend all students projects use git for their source code 
in this course. We've found it to have several benefits over SVN
or other version control systems.

## Free Online Git Respositories
* [BitBucket](https://bitbucket.org) - Can have private repositories with up to 5 members
* [GitLab](https://gitlab.com) - Can have private repositories with unlimited(?) members.
* [GitHub](https://github.com/) - Primarily for Open Source projects.

## Installation
Git can be installed on Mac, Windows, or Linux machines (if it is not already). You can
test by opening up a command prompt or terminal and running `git --version`. If you get
an error, you will likely need to install git on your machine. Information
for installation can be found 
[here](https://www.atlassian.com/git/tutorials/install-git/) or 
or
[here](https://git-scm.com/).

## Common Commands
Great list of commands and resource can be found [here](http://krishnaiitd.github.io/gitcommands/).

![Chart of Git Commands](http://krishnaiitd.github.io/gitcommands/images/GitWorkflow-3.png)

## GUI Tools
Here are a few GUI based interfaces for git which basically are running the console
commands for you. These are a few that students have liked in the past.

* [Atlassian SourceTree](https://www.sourcetreeapp.com/) - Students usually like this one.
* [Visual Studio Code](https://code.visualstudio.com/) - A simple/free IDE with basic Git integration 

Honestly, most decent IDEs have git integration.

## Other Resources/Links
* [Git For Ages 4 And Up](https://www.youtube.com/watch?v=1ffBJ4sVUb4) - Great Tutorial for All Ages
* [Git Workflows](https://www.atlassian.com/git/tutorials/comparing-workflows) - Good resource on workflows using git
